package com.wasdrqm.mvvm.domain.dataSources

import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.data.database.entities.TodayForecast
import kotlinx.coroutines.flow.Flow

interface DatabaseDataSources {

    suspend fun insertTodayForecast(today: TodayForecast)

    fun observeTodayForecast(city: String, lang: String): Flow<TodayForecast>

    suspend fun insertDayForecast(days: List<DayForecast>)

    fun observeDaysForecast(city: String, lang: String, time: Long): Flow<List<DayForecast>>

}