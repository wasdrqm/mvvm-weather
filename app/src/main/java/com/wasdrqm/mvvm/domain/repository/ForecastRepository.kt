package com.wasdrqm.mvvm.domain.repository

import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.data.database.entities.TodayForecast
import com.wasdrqm.mvvm.data.network.responses.ForecastForTodayResponse
import com.wasdrqm.mvvm.data.network.responses.ForecastForFewDaysResponse
import kotlinx.coroutines.flow.Flow

interface ForecastRepository {

    fun observeTodayForecast(city: String, lang: String): Flow<TodayForecast>
    fun observeFewDaysForecast(city: String, lang: String): Flow<List<DayForecast>>

    suspend fun fetchTodayForecast(city: String, lang: String): ForecastForTodayResponse
    suspend fun fetchFewDaysForecast(city: String, lang: String): ForecastForFewDaysResponse

}