package com.wasdrqm.mvvm.domain.dataSources

import com.wasdrqm.mvvm.data.network.responses.ForecastForTodayResponse
import com.wasdrqm.mvvm.data.network.responses.ForecastForFewDaysResponse

interface NetworkForecastDataSources {

    suspend fun fetchToday(city: String, lang: String): ForecastForTodayResponse
    suspend fun fetchFewDays(city: String, lang: String): ForecastForFewDaysResponse

}