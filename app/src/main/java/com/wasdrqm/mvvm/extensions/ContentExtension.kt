package com.wasdrqm.mvvm.extensions

import android.content.Context
import com.wasdrqm.mvvm.R
import com.wasdrqm.mvvm.core.MVVMApplication
import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.data.database.entities.TodayForecast
import com.wasdrqm.mvvm.data.network.responses.ForecastForTodayResponse
import com.wasdrqm.mvvm.data.network.responses.ForecastForFewDaysResponse
import com.wasdrqm.mvvm.data.network.responses.Weathers
import java.util.*

fun Long.toDatetime() = (if (this > 0) Date(this * 1000L) else Date()).time

fun String.toCityNameResString(context: Context) = when(this) {
    MVVMApplication.MOSCOW -> context.getString(R.string.city_moscow)
    MVVMApplication.NOVOSIBIRSK -> context.getString(R.string.city_novosibirsk)
    MVVMApplication.TASHKENT -> context.getString(R.string.city_tashkent)
    else -> null
}

fun ForecastForTodayResponse.toTodayForecast(city: String, lang: String) = TodayForecast(
    this@toTodayForecast.datetime.toDatetime(), city, lang, weather?.firstOrNull()?.description,
    this@toTodayForecast.main?.temperature, this@toTodayForecast.main?.temperatureMin,
    this@toTodayForecast.main?.temperatureMax, this@toTodayForecast.main?.humidity,
    this@toTodayForecast.main?.pressure, this@toTodayForecast.weatherSystem?.sunset?.toDatetime(),
    this@toTodayForecast.weatherSystem?.sunrise?.toDatetime()
)

fun ForecastForFewDaysResponse.toDayWeathers(
    city: String, lang: String
) = mutableListOf<DayForecast>()
    .apply {
        this@toDayWeathers.weathers?.forEach { weathers -> add(weathers.toDayForecast(city, lang)) }
    }

fun Weathers.toDayForecast(city: String, lang: String) = DayForecast(
    this@toDayForecast.datetime.toDatetime(), city, lang,
    this@toDayForecast.weathers?.firstOrNull()?.icon, this@toDayForecast.weathers?.firstOrNull()?.description,
    this@toDayForecast.temperature?.morning, this@toDayForecast.temperature?.day,
    this@toDayForecast.temperature?.eve, this@toDayForecast.temperature?.night,
)

fun getCurrentLanguage(): String = Locale.getDefault().language

fun Context.getErrorMessage(errorCode: Int) = getString(when(errorCode) {
    in 400..504 -> R.string.error_value_api_value_null
    0 -> R.string.error_value_no_internet_access
    else -> R.string.error_value_insert_in_db
})