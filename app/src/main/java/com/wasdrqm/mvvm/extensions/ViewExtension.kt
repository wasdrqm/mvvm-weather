package com.wasdrqm.mvvm.extensions

import android.content.res.Resources
import android.util.TypedValue

fun Resources.convertValueToDp(value: Int) = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP, value.toFloat(), this.displayMetrics
)

fun Resources.roundConvertValueToDp(value: Int) = this.convertValueToDp(value).toInt()