package com.wasdrqm.mvvm.extensions

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.retryWhen
import retrofit2.HttpException
import java.io.IOException
import java.net.UnknownHostException

fun <T> Flow<T>.retryWhenIOWithDelay(duration: Long, maxRetries: Int) = this
    .retryWhen { cause, attempt ->
        if (cause is IOException && attempt < maxRetries) {
            delay(duration)
            true
        } else {
            false
        }
    }

fun Throwable.toErrorCode() = when(this) {
    is HttpException -> this.code()
    is UnknownHostException -> 0
    else -> -1
}