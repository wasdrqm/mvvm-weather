package com.wasdrqm.mvvm.utils

import com.wasdrqm.mvvm.core.MVVMApplication
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    const val DATE_FORMAT_DATE = "yyyy.MM.dd"

    private const val DATE_FORMAT_TIME = "HH:mm"
    private const val MOSCOW = "Europe/Moscow"
    private const val NOVOSIBIRSK = "Asia/Novosibirsk"
    private const val TASHKENT = "Asia/Tashkent"

    fun getFormatDateTime(
        datetime: Long, city: String?, format: String? = DATE_FORMAT_TIME
    ): String = SimpleDateFormat(format, Locale.getDefault()).apply {
        timeZone = TimeZone.getTimeZone(when (city) {
            MVVMApplication.MOSCOW -> MOSCOW
            MVVMApplication.NOVOSIBIRSK -> NOVOSIBIRSK
            MVVMApplication.TASHKENT -> TASHKENT
            else -> TimeZone.getDefault().displayName
        })
    }.format(Date(datetime))

    fun todayTime() = Calendar.getInstance().apply {
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
    }.time.time

}