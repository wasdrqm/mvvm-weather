package com.wasdrqm.mvvm.utils

import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.wasdrqm.mvvm.extensions.roundConvertValueToDp

class MarginItemDecorator : RecyclerView.ItemDecoration {

    private var topOffset: Int = 0
    private var bottomOffset: Int = 0
    private var leftOffset: Int = 0
    private var rightOffset: Int = 0
    private val resources: Resources

    constructor(offSet: Int, resources: Resources) {
        bottomOffset = offSet
        leftOffset = offSet
        rightOffset = offSet
        topOffset = offSet
        this.resources = resources
    }

    constructor(topOffset: Int, bottomOffset: Int, leftOffset: Int, rightOffset: Int, resources: Resources) {
        this.bottomOffset = bottomOffset
        this.leftOffset = leftOffset
        this.rightOffset = rightOffset
        this.topOffset = topOffset
        this.resources = resources
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.left = resources.roundConvertValueToDp(leftOffset)
        outRect.right = resources.roundConvertValueToDp(rightOffset)
        outRect.top = resources.roundConvertValueToDp(topOffset)
        outRect.bottom = resources.roundConvertValueToDp(bottomOffset)
    }

}