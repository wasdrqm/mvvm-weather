package com.wasdrqm.mvvm.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.data.database.entities.TodayForecast
import kotlinx.coroutines.flow.Flow

@Dao
interface ForecastDao {

    // today

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTodayForecast(today: TodayForecast)

    @Query("SELECT * FROM today_forecast WHERE city = :city AND lang =:lang")
    fun observeTodayForecast(city: String, lang: String): Flow<TodayForecast>

    // few days

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDayForecast(days: List<DayForecast>)

    @Query(
        "SELECT * FROM day_forecast WHERE datetime >= :time AND city =:city AND lang =:lang ORDER BY datetime ASC LIMIT 5"
    )
    fun observeDaysForecast(city: String, lang: String, time: Long): Flow<List<DayForecast>>

}