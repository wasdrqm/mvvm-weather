package com.wasdrqm.mvvm.data.network.requests

import com.wasdrqm.mvvm.data.network.responses.ForecastForTodayResponse
import com.wasdrqm.mvvm.data.network.responses.ForecastForFewDaysResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ForecastApiRequests {

    @GET("/data/2.5/weather")
    suspend fun fetchForecastForToday(
        @Query("q") city: String?,
        @Query("lang") lang: String,
        @Query("mode") mode: String,
        @Query("units") units: String,
        @Query("appid") appId: String
    ): Response<ForecastForTodayResponse>

    @GET("/data/2.5/forecast/daily")
    suspend fun fetchForecastForFewDays(
        @Query("q") city: String?,
        @Query("lang") lang: String,
        @Query("mode") mode: String,
        @Query("units") units: String,
        @Query("cnt") counter: Int?,
        @Query("appid") appId: String,
    ): Response<ForecastForFewDaysResponse>

}