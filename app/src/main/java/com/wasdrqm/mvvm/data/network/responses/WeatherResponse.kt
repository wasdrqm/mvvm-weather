package com.wasdrqm.mvvm.data.network.responses

import com.google.gson.annotations.SerializedName

/**
 * User: wasdrqm;
 * Date: 28.11.2015;
 */
data class ForecastForTodayResponse(
    @SerializedName("id") var id: Long = 0,
    @SerializedName("dt")var datetime: Long = 0,
    @SerializedName("name") var name: String? = null,
    @SerializedName("cod") var code: Int = 0,
    @SerializedName("coord") var coordinates: Coordinates? = null,
    @SerializedName("weather") var weather: List<Weather>? = null,
    @SerializedName("main") var main: Main? = null,
    @SerializedName("wind") var wind: Wind? = null,
    @SerializedName("clouds") var clouds: Clouds? = null,
    @SerializedName("rain") var rain: Rain? = null,
    @SerializedName("snow") var snow: Snow? = null,
    @SerializedName("sys") var weatherSystem: WeatherSystem? = null,
)

data class ForecastForFewDaysResponse(
    @SerializedName("code") var code: Int = 0,
    @SerializedName("city") var city: City? = null,
    @SerializedName("list") var weathers: List<Weathers>? = null
)

data class City(
    @SerializedName("id") var id: Long = 0,
    @SerializedName("name") var name: String? = null,
    @SerializedName("country") var country: String? = null
)

data class TimezoneResponse(
    @SerializedName("timezoneId") val timezone: String? = null,
    @SerializedName("countryCode") val countryCode: String? = null,
    @SerializedName("countryName") val countryName: String? = null
)

data class Coordinates(
    @SerializedName("lat") var latitude: Double? = null,
    @SerializedName("lon") var longitude: Double? = null
)

data class Main(
    @SerializedName("temp") var temperature: Double? = null,
    @SerializedName("pressure") var pressure: Double? = null,
    @SerializedName("humidity") var humidity: Int = 0,
    @SerializedName("temp_min") var temperatureMin: Double? = null,
    @SerializedName("temp_max") var temperatureMax: Double? = null,
    @SerializedName("sea_level") var seaLevel: Double? = null,
    @SerializedName("grnd_level") var grndLevel: Double? = null
)

data class Clouds(var all: Int = 0)

data class Rain(
    @SerializedName("3h") var value: Double? = null
)

data class Snow(
    @SerializedName("3h") var value: Double? = null
)

data class Temperature(
    @SerializedName("day") var day: Double? = null,
    @SerializedName("min") var min: Double? = null,
    @SerializedName("max") var max: Double? = null,
    @SerializedName("night") var night: Double? = null,
    @SerializedName("eve") var eve: Double? = null,
    @SerializedName("morn") var morning: Double? = null
)

data class Weather(
    @SerializedName("id") var id: Int = 0,
    @SerializedName("main") var main: String? = null,
    @SerializedName("description") var description: String? = null,
    @SerializedName("icon") var icon: String? = null
)

data class Weathers(
    @SerializedName("dt") var datetime: Long = 0,
    @SerializedName("temp") var temperature: Temperature? = null,
    @SerializedName("pressure") var pressure: Double? = null,
    @SerializedName("humidity") var humidity: Int = 0,
    @SerializedName("speed") var speed: Double? = null,
    @SerializedName("deg") var deg: Int = 0,
    @SerializedName("clouds") var clouds: Int = 0,
    @SerializedName("weather") var weathers: List<Weather>? = null
)

data class WeatherSystem(
    @SerializedName("message") var message: Double? = null,
    @SerializedName("country") var country: String? = null,
    @SerializedName("sunrise") var sunrise: Long = 0,
    @SerializedName("sunset") var sunset: Long = 0
)

data class Wind(
    @SerializedName("speed") var speed: Double? = null,
    @SerializedName("deg") var degree: Double? = null
)
