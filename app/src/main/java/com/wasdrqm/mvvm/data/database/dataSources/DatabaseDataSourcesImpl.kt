package com.wasdrqm.mvvm.data.database.dataSources

import com.wasdrqm.mvvm.data.database.dao.ForecastDao
import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.data.database.entities.TodayForecast
import com.wasdrqm.mvvm.domain.dataSources.DatabaseDataSources
import kotlinx.coroutines.flow.Flow

class DatabaseDataSourcesImpl(private val forecastDao: ForecastDao) : DatabaseDataSources {

    override suspend fun insertTodayForecast(today: TodayForecast) {
        forecastDao.insertTodayForecast(today)
    }

    override fun observeTodayForecast(
        city: String, lang: String
    ): Flow<TodayForecast> = forecastDao.observeTodayForecast(city, lang)

    override suspend fun insertDayForecast(days: List<DayForecast>) {
        forecastDao.insertDayForecast(days)
    }

    override fun observeDaysForecast(
        city: String, lang: String, time: Long
    ): Flow<List<DayForecast>> = forecastDao.observeDaysForecast(city, lang, time)

}