package com.wasdrqm.mvvm.data.database.entities

import androidx.room.Entity

@Entity(
    tableName = "day_forecast",
    primaryKeys = ["datetime", "city", "lang"]
)
data class DayForecast(
    val datetime: Long,
    val city: String,
    val lang: String,
    val icon: String?,
    val description: String?,
    val morningTemperature: Double?,
    val dayTemperature: Double?,
    val eveningTemperature: Double?,
    val nightTemperature: Double?,
)