package com.wasdrqm.mvvm.data.network.dataSources

import com.wasdrqm.mvvm.data.network.requests.ForecastApiRequests
import com.wasdrqm.mvvm.domain.dataSources.NetworkForecastDataSources
import retrofit2.HttpException
import retrofit2.Response

class NetworkForecastDataSourcesImpl(
    private val appId: String,
    private val apiRequests: ForecastApiRequests
) : NetworkForecastDataSources {

    companion object {
        private const val DATA_FORMAT = "json"
        private const val UNITS_FORMAT = "metric"
        private const val COUNT: Int = 5
    }

    override suspend fun fetchToday(city: String, lang: String) = getResult {
        apiRequests.fetchForecastForToday(city, lang, DATA_FORMAT, UNITS_FORMAT, appId)
    }

    override suspend fun fetchFewDays(city: String, lang: String) = getResult {
        apiRequests.fetchForecastForFewDays(city, lang, DATA_FORMAT, UNITS_FORMAT, COUNT, appId)
    }

    private suspend fun <T> getResult(call: suspend () -> Response<T>): T {
        val response = call()
        return if (response.isSuccessful) {
            response.body() ?: throw NullPointerException("#retrofit response body is null")
        } else {
            throw HttpException(response)
        }
    }

}