package com.wasdrqm.mvvm.data.repositories

import com.wasdrqm.mvvm.domain.dataSources.DatabaseDataSources
import com.wasdrqm.mvvm.domain.dataSources.NetworkForecastDataSources
import com.wasdrqm.mvvm.data.network.responses.ForecastForTodayResponse
import com.wasdrqm.mvvm.data.network.responses.ForecastForFewDaysResponse
import com.wasdrqm.mvvm.domain.repository.ForecastRepository
import com.wasdrqm.mvvm.extensions.toTodayForecast
import com.wasdrqm.mvvm.extensions.toDayWeathers
import com.wasdrqm.mvvm.utils.DateTimeUtils
import javax.inject.Inject

class ForecastRepositoryImpl @Inject constructor(
    private val networkForecastDataSources: NetworkForecastDataSources,
    private val databaseDataSources: DatabaseDataSources
) : ForecastRepository {

    // from db block

    override fun observeTodayForecast(city: String, lang: String) = databaseDataSources
        .observeTodayForecast(city, lang)

    override fun observeFewDaysForecast(city: String, lang: String) = databaseDataSources
        .observeDaysForecast(city, lang, DateTimeUtils.todayTime())

    // from network block

    override suspend fun fetchTodayForecast(city: String, lang: String): ForecastForTodayResponse {
        val response = networkForecastDataSources.fetchToday(city, lang)
        databaseDataSources.insertTodayForecast(response.toTodayForecast(city, lang))
        return response
    }

    override suspend fun fetchFewDaysForecast(city: String, lang: String): ForecastForFewDaysResponse {
        val response = networkForecastDataSources.fetchFewDays(city, lang)
        databaseDataSources.insertDayForecast(response.toDayWeathers(city, lang))
        return response
    }

}