package com.wasdrqm.mvvm.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.wasdrqm.mvvm.data.database.dao.ForecastDao
import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.data.database.entities.TodayForecast

@Database(entities = [TodayForecast::class, DayForecast::class], version = 1, exportSchema = false)
abstract class ForecastDatabase : RoomDatabase() {

    abstract fun forecastDao(): ForecastDao

}