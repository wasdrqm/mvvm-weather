package com.wasdrqm.mvvm.data.database.entities

import androidx.room.Entity

@Entity(
    tableName = "today_forecast",
    primaryKeys = ["city", "lang"]
)
data class TodayForecast(
    val datetime: Long,
    val city: String,
    val lang: String,
    val description: String?,
    val temperature: Double?,
    val minTemperature: Double?,
    val maxTemperature: Double?,
    val humidity: Int?,
    val pressure: Double?,
    val sunset: Long?,
    val sunrise: Long?,
)