package com.wasdrqm.mvvm.ui.screens.forecast.today

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.wasdrqm.mvvm.R
import com.wasdrqm.mvvm.ui.states.ScreenResultState
import com.wasdrqm.mvvm.data.database.entities.TodayForecast
import com.wasdrqm.mvvm.databinding.FragmentTodayBinding
import com.wasdrqm.mvvm.extensions.getErrorMessage
import com.wasdrqm.mvvm.extensions.toCityNameResString
import com.wasdrqm.mvvm.utils.DateTimeUtils
import com.wasdrqm.mvvm.ui.base.BaseFragment
import com.wasdrqm.mvvm.ui.screens.forecast.days.FewDaysForecastFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
@OptIn(ExperimentalCoroutinesApi::class)
class TodayFragment : BaseFragment<FragmentTodayBinding>() {

    companion object {
        const val ARG_CITY = "arg::city"
    }

    // base fields

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentTodayBinding
        get() = FragmentTodayBinding::inflate

    // private fields

    private val viewModel by viewModels<TodayViewModel>()

    private var cityName: String? = null

    // base methods

    override fun initOrRestoreArgs(bundle: Bundle?) {
        cityName = bundle?.getString(ARG_CITY, null)
    }

    override fun saveArgs(bundle: Bundle) {
        bundle.putString(ARG_CITY, cityName)
    }

    override fun initViews() {
        binding.toolbar.title = cityName?.toCityNameResString(binding.toolbar.context)
        binding.swipeRefreshLayout.setOnRefreshListener { viewModel.update() }
        binding.buttonForecast.setOnClickListener(Navigation.createNavigateOnClickListener(
            R.id.action_todayFragment_to_fewDaysForecastFragment,
            bundleOf(FewDaysForecastFragment.ARG_CITY to cityName)
        ))
    }

    override fun workWithsFlows() {
        viewModel.dataFlow
            .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach(this::showForecast)
            .launchIn(lifecycleScope)

        viewModel.fetchResultFlow
            .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach(this::showResultState)
            .launchIn(lifecycleScope)
    }

    // custom methods

    private fun showResultState(result: ScreenResultState) {
        when(result) {
            is ScreenResultState.Loading -> {
                binding.swipeRefreshLayout.isRefreshing = true
            }
            is ScreenResultState.Success<*> -> {
                binding.swipeRefreshLayout.isRefreshing = false
            }
            is ScreenResultState.Error -> {
                binding.swipeRefreshLayout.isRefreshing = false
                Toast.makeText(
                    requireContext(),
                    requireContext().getErrorMessage(result.errorCode),
                    Toast.LENGTH_SHORT
                ).show()
            }
            is ScreenResultState.Init -> {
                binding.swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    private fun showForecast(today: TodayForecast) {
        binding.tvLastUpdate.text = DateTimeUtils.getFormatDateTime(today.datetime, cityName)
        binding.tvCurrentWeather.text = today.description
        binding.tvTemp.text = getString(R.string.format_celsius, today.temperature?.toInt())
        binding.tvMinTemp.text = getString(R.string.format_celsius, today.minTemperature?.toInt())
        binding.tvMaxTemp.text = getString(R.string.format_celsius, today.maxTemperature?.toInt())
        binding.tvHumidity.text = today.humidity.toString()
        binding.tvPressure.text = today.pressure.toString()
        today.sunset?.let { date ->
            binding.tvSunset.text = DateTimeUtils.getFormatDateTime(date, cityName)
        }
        today.sunrise?.let { date ->
            binding.tvSunrise.text = DateTimeUtils.getFormatDateTime(date, cityName)
        }
    }

}