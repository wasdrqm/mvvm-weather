package com.wasdrqm.mvvm.ui.screens.forecast.days

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.wasdrqm.mvvm.R
import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.databinding.ItemForecastBinding
import com.wasdrqm.mvvm.utils.DateTimeUtils

class ForecastAdapter(private val city: String?)
    : ListAdapter<DayForecast, ForecastAdapter.ForecastViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<DayForecast>() {
            override fun areItemsTheSame(oldItem: DayForecast, newItem: DayForecast) = oldItem.datetime == newItem.datetime
            override fun areContentsTheSame(oldItem: DayForecast, newItem: DayForecast) = oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ForecastViewHolder(
        ItemForecastBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        holder.bindData(currentList[position])
    }

    override fun getItemCount() = currentList.size

    fun updateData(weathers: List<DayForecast>) {
        submitList(weathers)
    }

    inner class ForecastViewHolder(
        private val binding: ItemForecastBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindData(weather: DayForecast) {
            binding.tvWeatherDescription.text = weather.description
            binding.ivWeather.load(itemView.context.getString(R.string.format_icon, weather.icon))
            binding.tvDateTime.text = weather.datetime.let { date ->
                DateTimeUtils.getFormatDateTime(date, city, DateTimeUtils.DATE_FORMAT_DATE)
            }
            binding.tvTempDay.text = itemView.context.getString(
                R.string.format_celsius, weather.dayTemperature?.toInt()
            )
            binding.tvTempMorning.text = itemView.context.getString(
                R.string.format_celsius, weather.morningTemperature?.toInt()
            )
            binding.tvTempEvening.text = itemView.context.getString(
                R.string.format_celsius, weather.eveningTemperature?.toInt()
            )
            binding.tvTempNight.text = itemView.context.getString(
                R.string.format_celsius, weather.nightTemperature?.toInt()
            )
        }

    }

}