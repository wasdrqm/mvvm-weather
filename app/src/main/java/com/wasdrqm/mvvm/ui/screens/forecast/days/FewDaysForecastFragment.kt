package com.wasdrqm.mvvm.ui.screens.forecast.days

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.wasdrqm.mvvm.R
import com.wasdrqm.mvvm.ui.states.ScreenResultState
import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.databinding.FragmentFewDaysForecastBinding
import com.wasdrqm.mvvm.extensions.getErrorMessage
import com.wasdrqm.mvvm.extensions.toCityNameResString
import com.wasdrqm.mvvm.utils.MarginItemDecorator
import com.wasdrqm.mvvm.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


@AndroidEntryPoint
@OptIn(ExperimentalCoroutinesApi::class)
class FewDaysForecastFragment : BaseFragment<FragmentFewDaysForecastBinding>() {

    companion object {
        const val ARG_CITY = "arg::city"
    }

    // base fields

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentFewDaysForecastBinding
        get() = FragmentFewDaysForecastBinding::inflate

    // private fields

    private val viewModel: FewDaysForecastViewModel by viewModels()
    private val forecastAdapter by lazy { ForecastAdapter(cityName) }

    private var cityName: String? = null

    // base methods

    override fun initOrRestoreArgs(bundle: Bundle?) {
        cityName = bundle?.getString(ARG_CITY, null)
    }

    override fun saveArgs(bundle: Bundle) {
        bundle.putString(ARG_CITY, cityName)
    }

    override fun initViews() {
        binding.toolbar.setNavigationOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.fragment_container).popBackStack()
        }
        binding.toolbar.title = cityName?.toCityNameResString(binding.toolbar.context)
        binding.rvWeather.apply {
            layoutManager = LinearLayoutManager(binding.rvWeather.context)
            itemAnimator = DefaultItemAnimator()
            addItemDecoration(MarginItemDecorator(8, resources))
            adapter = forecastAdapter
        }
        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.update()
        }
    }

    override fun workWithsFlows() {
        viewModel.dataFlow
            .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach(this::showWeathers)
            .launchIn(lifecycleScope)

        viewModel.fetchResultFlow
            .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach(this::showResultState)
            .launchIn(lifecycleScope)
    }

    // custom methods

    private fun showResultState(result: ScreenResultState) {
        when(result) {
            is ScreenResultState.Loading -> {
                binding.swipeRefreshLayout.isRefreshing = true
            }
            is ScreenResultState.Success<*> -> {
                binding.swipeRefreshLayout.isRefreshing = false
            }
            is ScreenResultState.Error -> {
                binding.swipeRefreshLayout.isRefreshing = false
                Toast.makeText(
                    requireContext(),
                    requireContext().getErrorMessage(result.errorCode),
                    Toast.LENGTH_SHORT
                ).show()
            }
            is ScreenResultState.Init -> {
                binding.swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    private fun showWeathers(weathers: List<DayForecast>) {
        forecastAdapter.updateData(weathers)
    }

}