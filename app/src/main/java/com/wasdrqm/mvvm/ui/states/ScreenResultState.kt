package com.wasdrqm.mvvm.ui.states

sealed class ScreenResultState {
    data class Success<out T>(val value: T): ScreenResultState()
    data class Error(val message: String?, val errorCode: Int): ScreenResultState()
    object Loading : ScreenResultState()
    object Init : ScreenResultState()
}