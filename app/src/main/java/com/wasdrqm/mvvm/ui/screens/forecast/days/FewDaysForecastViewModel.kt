package com.wasdrqm.mvvm.ui.screens.forecast.days

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.ajalt.timberkt.e
import com.wasdrqm.mvvm.domain.repository.ForecastRepository
import com.wasdrqm.mvvm.ui.states.ScreenResultState
import com.wasdrqm.mvvm.data.database.entities.DayForecast
import com.wasdrqm.mvvm.extensions.getCurrentLanguage
import com.wasdrqm.mvvm.extensions.toErrorCode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class FewDaysForecastViewModel @Inject constructor(
    private val forecastRepository: ForecastRepository,
    private val state: SavedStateHandle
) : ViewModel() {

    private val _fetchResultFlow = MutableStateFlow<ScreenResultState>(ScreenResultState.Init)
    private val _dataFlow = MutableStateFlow<List<DayForecast>?>(null)

    private var fetchJob: Job? = null

    private val city: String? by lazy {
        state.get<String>(FewDaysForecastFragment.ARG_CITY)
    }

    val fetchResultFlow = _fetchResultFlow.asStateFlow()
    val dataFlow = _dataFlow.asStateFlow().filterNotNull()

    init {
        // from db
        city?.let(this::observeForecast)
        // from network
        city?.let(this::fetchForecast)
    }

    fun update() {
        city?.let(this::fetchForecast)
    }

    private fun observeForecast(city: String) {
        forecastRepository
            .observeFewDaysForecast(city, getCurrentLanguage())
            .catch { throwable -> e(throwable) { "#few_days error observe forecast" } }
            .onEach(_dataFlow::emit)
            .flowOn(Dispatchers.IO)
            .launchIn(viewModelScope)
    }

    private fun fetchForecast(city: String) {
        fetchJob?.cancel()
        fetchJob = viewModelScope.launch(Dispatchers.IO) {
            runCatching {
                _fetchResultFlow.emit(ScreenResultState.Loading)
                forecastRepository.fetchFewDaysForecast(city, getCurrentLanguage())
            }.onSuccess { response ->
                _fetchResultFlow.emit(ScreenResultState.Success(response))
            }.onFailure { throwable ->
                e(throwable) { "#few_days error fetch forecast" }
                _fetchResultFlow.emit(ScreenResultState.Error(
                    "Error update forecast on few days", throwable.toErrorCode()
                ))
            }
        }
    }

}