package com.wasdrqm.mvvm.ui.screens.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import com.wasdrqm.mvvm.R
import com.wasdrqm.mvvm.core.MVVMApplication
import com.wasdrqm.mvvm.databinding.FragmentMainBinding
import com.wasdrqm.mvvm.ui.base.BaseFragment
import com.wasdrqm.mvvm.ui.screens.forecast.today.TodayFragment

class MainFragment : BaseFragment<FragmentMainBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentMainBinding
        get() = FragmentMainBinding::inflate

    override fun initOrRestoreArgs(bundle: Bundle?) {
        // do nothing
    }

    override fun saveArgs(bundle: Bundle) {
        // do nothing
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.buttonMoscow.setOnClickListener(createClickListener(MVVMApplication.MOSCOW))
        binding.buttonNovosibirsk.setOnClickListener(createClickListener(MVVMApplication.NOVOSIBIRSK))
        binding.buttonTashkent.setOnClickListener(createClickListener(MVVMApplication.TASHKENT))
    }

    private fun createClickListener(city: String) = Navigation.createNavigateOnClickListener(
        R.id.action_mainFragment_to_todayFragment,
        bundleOf(TodayFragment.ARG_CITY to city)
    )

}