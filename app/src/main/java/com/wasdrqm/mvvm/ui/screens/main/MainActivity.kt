package com.wasdrqm.mvvm.ui.screens.main

import android.os.Bundle
import android.view.LayoutInflater
import com.wasdrqm.mvvm.databinding.ActivityMainBinding
import com.wasdrqm.mvvm.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding
        get() = ActivityMainBinding::inflate

    override fun initOrRestoreArgs(bundle: Bundle?) {
        // do thing
    }

    override fun saveArgs(bundle: Bundle) {
        // do nothing
    }

}