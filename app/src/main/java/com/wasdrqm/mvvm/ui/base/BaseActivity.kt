package com.wasdrqm.mvvm.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<B : ViewBinding> : AppCompatActivity() {

    private var _binding: B? = null

    @Suppress("UNCHECKED_CAST")
    protected open val binding get() = _binding as B
    abstract val bindingInflater: (LayoutInflater) -> B


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = bindingInflater.invoke(layoutInflater)
        setContentView(requireNotNull(_binding).root)

        if (savedInstanceState == null) {
            initOrRestoreArgs(intent.extras)
        } else {
            initOrRestoreArgs(savedInstanceState)
        }

        initViews()
        workWithsFlows()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        saveArgs(outState)
        super.onSaveInstanceState(outState)
    }

    /**
     * init or restore instance state
     */
    abstract fun initOrRestoreArgs(bundle: Bundle?)

    /**
     * save instance state
     */
    abstract fun saveArgs(bundle: Bundle)

    /**
     * init layout views and actions
     */
    open fun initViews() {}

    /**
     * init work with livedata here
     */
    open fun workWithsFlows() {}

}