package com.wasdrqm.mvvm.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<B: ViewBinding> : Fragment() {

    // abstract

    private var _binding: B? = null

    @Suppress("UNCHECKED_CAST")
    protected open val binding get() = _binding as B
    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> B

    // base

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        workWithsFlows()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = bindingInflater.invoke(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initOrRestoreArgs(savedInstanceState ?: requireArguments())
        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        saveArgs(outState)
        super.onSaveInstanceState(outState)
    }

    /**
     * init or restore instance state
     */
    abstract fun initOrRestoreArgs(bundle: Bundle?)

    /**
     * save instance state
     */
    abstract fun saveArgs(bundle: Bundle)

    /**
     * init layout views and actions
     */
    open fun initViews() {}

    /**
     * init variables here then you need set value from viewModel
     */
    open fun initVariables() {}

    /**
     * init work with flows here
     */
    open fun workWithsFlows() {}

}