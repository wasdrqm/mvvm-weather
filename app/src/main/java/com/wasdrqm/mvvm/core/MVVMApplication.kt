package com.wasdrqm.mvvm.core

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class MVVMApplication : Application() {

    companion object {
        const val MOSCOW = "Moscow"
        const val NOVOSIBIRSK = "Novosibirsk"
        const val TASHKENT = "Tashkent"
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

}