package com.wasdrqm.mvvm.core.di.modules

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.wasdrqm.mvvm.BuildConfig
import com.wasdrqm.mvvm.core.di.annotations.CustomOkHttpClient
import com.wasdrqm.mvvm.core.di.annotations.JsonConverterFactory
import com.wasdrqm.mvvm.data.network.requests.ForecastApiRequests
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ApiModule {

    companion object {
        private const val BASE_URL = "http://api.openweathermap.org"
    }

    @JsonConverterFactory
    @Provides
    fun provideJsonConverter(): Converter.Factory = GsonConverterFactory.create(GsonBuilder().apply {
        setLenient()
        setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
    }.create())

    @CustomOkHttpClient
    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder().apply {
        if (BuildConfig.DEBUG) {
            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            })
        }
        connectTimeout(10, TimeUnit.SECONDS)
        readTimeout(10, TimeUnit.SECONDS)
    }.build()

    @Singleton
    @Provides
    fun provideApi(
        @JsonConverterFactory converter: Converter.Factory,
        @CustomOkHttpClient client: OkHttpClient
    ): ForecastApiRequests = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(converter)
        .build()
        .create(ForecastApiRequests::class.java)

}