package com.wasdrqm.mvvm.core.di.modules

import com.wasdrqm.mvvm.BuildConfig
import com.wasdrqm.mvvm.domain.repository.ForecastRepository
import com.wasdrqm.mvvm.data.repositories.ForecastRepositoryImpl
import com.wasdrqm.mvvm.domain.dataSources.DatabaseDataSources
import com.wasdrqm.mvvm.data.database.dataSources.DatabaseDataSourcesImpl
import com.wasdrqm.mvvm.data.database.dao.ForecastDao
import com.wasdrqm.mvvm.domain.dataSources.NetworkForecastDataSources
import com.wasdrqm.mvvm.data.network.dataSources.NetworkForecastDataSourcesImpl
import com.wasdrqm.mvvm.data.network.requests.ForecastApiRequests
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DataSourcesModule {

    @Singleton
    @Provides
    fun provideApiDataSources(
        apiRequests: ForecastApiRequests
    ): NetworkForecastDataSources = NetworkForecastDataSourcesImpl(BuildConfig.APP_ID, apiRequests)

    @Singleton
    @Provides
    fun provideDbDataSources(
        forecastDao: ForecastDao
    ): DatabaseDataSources = DatabaseDataSourcesImpl(forecastDao)

    @Singleton
    @Provides
    fun provideDataRepository(
        networkForecastDataSources: NetworkForecastDataSources,
        databaseDataSources: DatabaseDataSources
    ): ForecastRepository = ForecastRepositoryImpl(networkForecastDataSources, databaseDataSources)

}