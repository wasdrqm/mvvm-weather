package com.wasdrqm.mvvm.core.di.annotations

import javax.inject.Qualifier


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class JsonConverterFactory

@Qualifier
@Retention
annotation class CustomOkHttpClient