package com.wasdrqm.mvvm.core.di.modules

import android.content.Context
import androidx.room.Room
import com.wasdrqm.mvvm.data.database.ForecastDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideRoomDb(@ApplicationContext context: Context): ForecastDatabase = Room
        .inMemoryDatabaseBuilder(context, ForecastDatabase::class.java)
        .fallbackToDestructiveMigration()
        .build()

    @Singleton
    @Provides
    fun provideWeatherDao(db: ForecastDatabase) = db.forecastDao()

}